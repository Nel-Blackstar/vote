package com.im;

import com.im.core.repository.UsersRepository;
import com.im.core.repository.RolesRepository;
import com.im.core.entities.Users;
import com.im.core.entities.Roles;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class InventoryManagerApplication {
	 
	public static void main(String[] args) {
		ApplicationContext ctx=SpringApplication.run(InventoryManagerApplication.class, args);
		UsersRepository usersRepository=ctx.getBean(UsersRepository.class);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		Users user=new Users("admin","admin");
		user.setActive(true);
		user.setUsername("admin");
		user.setPassword(encoder.encode(user.getPassword()));
		RolesRepository rolesRepository=ctx.getBean(RolesRepository.class);
		Roles role=new Roles();
		role.setNom_role("ADMIN");
		rolesRepository.save(role);
		Roles gestR=new Roles();
		gestR.setNom_role("GESTIONNAIRE");
		rolesRepository.save(gestR);
		//usersRepository.save(user);
		Users gest=new Users("etudiant","etudiant");
		gest.setActive(true);
		gest.setUsername("gest");
		gest.setPassword(encoder.encode(gest.getPassword()));
		Users savesdUser=usersRepository.save(gest);
        // Enregistrer l'utilisateur
        Users savedUser = usersRepository.save(user);
        // Le r�le selectionn� existe dans le syst�mes
        savedUser.addFirstRole(role);
        savesdUser.addFirstRole(gestR);
        usersRepository.save(savedUser);
        usersRepository.save(savesdUser);
	}

}
