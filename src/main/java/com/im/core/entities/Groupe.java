package com.im.core.entities;

import com.im.common.entities.ImEntity;

import javax.persistence.Entity;

@Entity
public class Groupe extends ImEntity {

	private String nomGroupe;
	private String hyperviseur;
    private Integer point;
    
    public Groupe() {
    	
    }
	public Groupe(String nomGroupe, String hyperviseur, Integer point) {
		super();
		this.nomGroupe = nomGroupe;
		this.hyperviseur = hyperviseur;
		this.point = point;
	}

	public String getNomGroupe() {
		return nomGroupe;
	}

	public void setNomGroupe(String nomGroupe) {
		this.nomGroupe = nomGroupe;
	}

	public String getHyperviseur() {
		return hyperviseur;
	}

	public void setHyperviseur(String hyperviseur) {
		this.hyperviseur = hyperviseur;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

    
   
}
