package com.im.core.entities;

import com.im.common.entities.Detail;
import com.im.common.entities.ImEntity;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
public class Etudiant extends ImEntity {

    private String nom;
    private Integer niveau;
    private String prenom;
    private String filiere;


	// List Detail
    @OneToOne(targetEntity = Groupe.class)
    Groupe groupe;

	public Etudiant() {
    	
    }

    public Etudiant(String nom, Integer niveau, String prenom, String filiere, Groupe groupe) {
		super();
		this.nom = nom;
		this.niveau = niveau;
		this.prenom = prenom;
		this.filiere = filiere;
		this.groupe = groupe;
	}
    
	public String getFiliere() {
		return filiere;
	}
	
	public void setFiliere(String filiere) {
		this.filiere = filiere;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Integer getNiveau() {
		return niveau;
	}

	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}

	public String getNom() {
		return nom;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
  
}
