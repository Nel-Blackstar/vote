package com.im.core.repository;

import com.im.core.entities.Parametre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParametreRepository extends JpaRepository<Parametre, Long> {
}
