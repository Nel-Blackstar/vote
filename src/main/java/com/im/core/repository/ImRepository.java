package com.im.core.repository;

import com.im.core.entities.Im;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImRepository extends JpaRepository<Im, Long> {
}
