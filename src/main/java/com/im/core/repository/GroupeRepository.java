package com.im.core.repository;

import com.im.core.entities.Groupe;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GroupeRepository extends JpaRepository<Groupe, Long> {
	@Query("SELECT g FROM Groupe g WHERE g.point<5 ORDER BY RAND()")
	List<Groupe> findActive();
}
