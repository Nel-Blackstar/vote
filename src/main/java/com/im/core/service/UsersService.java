package com.im.core.service;

import com.im.core.entities.Roles;
import com.im.core.entities.Users;

import java.util.List;

public interface UsersService {
    Users save(Users users);
    void delete(Users users);
    Users findOne(Long id);
    List<Users> findAll();
    Users findByLogin(String login);
    long utilisateursCount();
    List<Users> findUsersByRole(Roles roles);
}
