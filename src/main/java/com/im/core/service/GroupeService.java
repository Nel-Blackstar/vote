package com.im.core.service;

import com.im.core.entities.Groupe;

import java.util.List;

public interface GroupeService {
    Groupe save(Groupe groupe);
    void delete(Groupe groupe);
    Groupe findOne(Long id);
    List<Groupe> findAll();
    List<Groupe> findActive();
}
