package com.im.core.service;

import com.im.core.entities.Im;
import com.im.core.repository.ImRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImServiceImpl implements ImService{
    @Autowired
    ImRepository liveRepository;
    @Override
    public Im save(Im live) {
        return liveRepository.save(live);
    }

    @Override
    public void delete(Im live) {
        if (liveRepository.getOne(live.getId()) != null) {
            liveRepository.delete(live);
        } else {
            new RuntimeException("entity doesn't exist");
        }

    }

    @Override
    public Im findOne(Long id) {
        return liveRepository.getOne(id);
    }

    @Override
    public List<Im> findAll() {
        return liveRepository.findAll();
    }
}
