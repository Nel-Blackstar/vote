package com.im.core.service;

import com.im.core.entities.Groupe;
import com.im.core.repository.GroupeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupeServiceImpl implements GroupeService{
    @Autowired
    GroupeRepository groupeRepository;
    @Override
    public Groupe save(Groupe groupe) {
        return groupeRepository.save(groupe);
    }

    @Override
    public void delete(Groupe groupe) {
        if (groupeRepository.getOne(groupe.getId()) != null) {
            groupeRepository.delete(groupe);
        } else {
            new RuntimeException("entity doesn't exist");
        }

    }

    @Override
    public Groupe findOne(Long id) {
        return groupeRepository.getOne(id);
    }

    @Override
    public List<Groupe> findAll() {
        return groupeRepository.findAll();
    }
    @Override
    public List<Groupe> findActive() {
        return groupeRepository.findActive();
    }
}
