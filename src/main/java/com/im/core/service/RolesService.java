package com.im.core.service;

import com.im.core.entities.Roles;

import java.util.List;

public interface RolesService {
    Roles save(Roles roles);
    void delete(Roles roles);
    Roles findOne(String id);
    List<Roles> findAll();
}
