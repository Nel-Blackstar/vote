package com.im.core.service;

import com.im.core.entities.Users;

public interface IImManager {
    public Users userConnecte();

    public char getRandomLetterUppercase();
    public char getRandomLetterLowercase();

    public String genererLoginUser();
    public String genererPasswordUser();
    public String crypterPasswordUser(String password);
}
