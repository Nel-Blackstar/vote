package com.im.core.service;

import com.im.core.entities.Etudiant;
import com.im.core.repository.EtudiantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EtudiantServiceImpl implements EtudiantService{
    @Autowired
    EtudiantRepository etudiantRepository;
    @Override
    public Etudiant save(Etudiant etudiant) {
        return etudiantRepository.save(etudiant);
    }

    @Override
    public void delete(Etudiant etudiant) {
        if (etudiantRepository.getOne(etudiant.getId()) != null) {
            etudiantRepository.delete(etudiant);
        } else {
            new RuntimeException("entity doesn't exist");
        }

    }

    @Override
    public Etudiant findOne(Long id) {
        return etudiantRepository.getOne(id);
    }

    @Override
    public List<Etudiant> findAll() {
        return etudiantRepository.findAll();
    }
}
