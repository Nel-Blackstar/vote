package com.im.core.service;

import com.im.core.entities.Im;

import java.util.List;

public interface ImService {
    Im save(Im live);
    void delete(Im live);
    Im findOne(Long id);
    List<Im> findAll();
}
