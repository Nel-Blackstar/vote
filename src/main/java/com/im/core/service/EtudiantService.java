package com.im.core.service;

import com.im.core.entities.Etudiant;

import java.util.List;

public interface EtudiantService {
    Etudiant save(Etudiant etudiant);
    void delete(Etudiant etudiant);
    Etudiant  findOne(Long id);
    List<Etudiant> findAll();
}
