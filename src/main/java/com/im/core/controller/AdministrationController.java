package com.im.core.controller;

import com.im.core.entities.Im;
import com.im.core.entities.Roles;
import com.im.core.entities.Users;
import com.im.core.repository.UsersRepository;
import com.im.core.service.IImManager;
import com.im.core.service.ImService;
import com.im.core.service.RolesService;
import com.im.core.service.UsersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class AdministrationController {
    @Autowired
    ImService liveService;
    @Autowired
    UsersService usersService;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    RolesService rolesService;
    // Objet de cryptage et decryptage des mots de passe
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    // Rechercher l'autoecole et charger dans le modèle s'il existe, sinon, charger un autoecole par défaut
    public void chargerLive(Model model) {
        List<Im> lives = liveService.findAll();

        if (!lives.isEmpty()) {
            model.addAttribute("lives", lives.get(0));
        } else {
            Im default_live = new Im();
            model.addAttribute("live", default_live);
        }
    }
  //management des utilisateurs

    /** Ajout d'un user
     *
     * @param model
     * @return
     */
    @GetMapping(value="/users/ajouter-user")
    public String formUser(Model model) {
        chargerLive(model);
        // Charger la liste des r�les disponibles et d�poser dans le model
        model.addAttribute("listeRoles", rolesService.findAll());
        model.addAttribute("state", "get");
        model.addAttribute("users", new Users());
        return "administration/utilisateurs/create";
    }

    //gestion des userss

    /**
     * <b> methode d'ajout d'un utilisateur </b>
     * @param model
     * @param users utilisateur a ajouter
     * @return
     */
    @PostMapping(value = "/users/ajouter-user")
    public String ajouterUser(Model model, Users users, @RequestParam("role") String role,@RequestParam("vmdp") String vmdp) {
        //model.addAttribute("user", iHotelManager.userConnecte());
        chargerLive(model);

        Users user = new Users();
        user.setLogin(users.getLogin());
        user.setActive(true);
        user.setUsername(users.getUsername());

        // Crypter le mot de passe utilisateur
        user.setPassword(encoder.encode(users.getPassword()));

        // Attribuer les r�les � l'utilisateur
      Roles roles = rolesService.findOne(role);
        if(roles != null){
            // Enregistrer l'utilisateur
            Users savedUser = usersService.save(user);
            // Le r�le selectionn� existe dans le syst�me
            savedUser.addFirstRole(roles);
            usersService.save(savedUser);
        }
        model.addAttribute("state", "post");
        model.addAttribute("info",user.getLogin() +" - "+user.getUsername());
        return "redirect:/admin/users";
    }

    /**
     *     <b> Consultation d'un utilisateur </b>
     * @param model
     * @param login identifiant de l'users
     * @return
     */
    @RequestMapping("/users/consulter-user/{id}")
    public String consulterUser(Model model, @PathVariable("id") String login) {
        //model.addAttribute("user", iLiveManager.userConnecte());
        chargerLive(model);
        // Retrouver l'users � consulter et le placer dans le mod�le
        Users users = usersRepository.findUsersByLogin(login);;
        model.addAttribute("users", users);
        return "administration/utilisateurs/view";
    }

    // Modification des informations sur l'users

    /**
     *     <b>  Modification des informations sur l'utilisateur</b>
     * @param model
     * @param login identifiant de l'utilisateur
     * @return
     */
    @GetMapping("/users/editer-user/{id}")
    public String editerUser(Model model, @PathVariable("id") String login) {
        Users users = usersRepository.findUsersByLogin(login);
        model.addAttribute("state", "get");
        model.addAttribute("users", users);
        model.addAttribute("listeRoles", rolesService.findAll());
        //model.addAttribute("user", iLiveManager.userConnecte());
        chargerLive(model);
        return "administration/utilisateurs/update";
    }

    @PostMapping(value = "/users/editer-user")
    public String editUser(Model model, Users users, @RequestParam("role") String role,@RequestParam("vmdp") String vmdp) {
        //model.addAttribute("user", iHotelManager.userConnecte());
        chargerLive(model);

        Users user = usersRepository.findUsersByLogin(users.getLogin());
        user.setLogin(users.getLogin());
        user.setActive(true);
        user.setUsername(users.getUsername());

        // Crypter le mot de passe utilisateur
        user.setPassword(encoder.encode(users.getPassword()));

        // Attribuer les r�les � l'utilisateur
        Roles roles = rolesService.findOne(role);
        if(roles != null){
            // Enregistrer l'utilisateur
            Users savedUser = usersService.save(user);
            // Le r�le selectionn� existe dans le syst�mes
            savedUser.addFirstRole(roles);
            usersService.save(savedUser);
        }
        model.addAttribute("state", "post");
        model.addAttribute("info",user.getLogin() +" - "+user.getUsername());
        return "redirect:/admin/users";
    }

    // Suppression d'une users
    @RequestMapping("/users/supprimer-user/{id}")
    public String supprimerUser(Model model, @PathVariable("id") String login) {
        usersService.delete(usersRepository.findUsersByLogin(login));
        chargerLive(model);
        return "redirect:/admin/users";
    }

    /**
     * Rechercher de l'ensemble des utilisateurs du systeme
     * @param model
     * @return
     */
    @GetMapping("/users")
    public String listUsers(Model model) {
        model.addAttribute("listeUsers", usersService.findAll());
        //model.addAttribute("user", iHotelManager.userConnecte());
        chargerLive(model);

        return "administration/utilisateurs/index";
    }

}
