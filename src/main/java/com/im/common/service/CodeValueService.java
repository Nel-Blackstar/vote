package com.im.common.service;

import com.im.common.entities.Code;
import com.im.common.entities.CodeValue;

import java.util.List;

public interface CodeValueService {

    CodeValue save(CodeValue codeValue);

    void delete(CodeValue codeValue);

    CodeValue findOne(String id);
    CodeValue findById(Long id);

    List<CodeValue> findAll();

    CodeValue findByCodeValue(String codeValue);

    List<CodeValue> findCodeValueByCode(Code code);

    List<CodeValue> findByIdentifier(String identifier);
}
