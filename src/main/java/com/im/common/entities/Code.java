package com.im.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.im.common.entities.ImEntity;


@Entity
public class Code extends ImEntity {

    @Column(nullable = false)
    private String identifier;

    @Column(nullable = false)
    private String description;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
