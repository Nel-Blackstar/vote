package com.im.common.controller;

import com.im.core.entities.Groupe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.im.core.entities.Etudiant;
import com.im.core.entities.Roles;
import com.im.core.entities.Users;
import com.im.core.repository.RolesRepository;
import com.im.core.repository.UsersRepository;
import com.im.core.service.EtudiantService;
import com.im.core.service.GroupeServiceImpl;
import com.im.core.service.RolesService;
import com.im.core.service.UsersService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class IndexController {
    @Autowired
    UsersRepository usersRepository;
	@Autowired
    UsersService usersService;
	@Autowired
    EtudiantService etudiantsService;
	@Autowired
    RolesService rolesService;
	@Autowired
    GroupeServiceImpl groupesService;
    // Point d'entrée dans l'application
    @GetMapping("/")
    public String home(Model model) {
        return "index";
    }
    @GetMapping("/home")
    public String homeSecond(Model model) {
        return "home";
    }
    
    // Acces à l'interface du personnel
    @GetMapping("/administrateur")
    public String administration(Model model,HttpSession session) {
        if (session.getAttribute("infos")!=null){
            model.addAttribute("info",session.getAttribute("infos"));
            session.removeAttribute("infos");
        }
        List<Groupe> groupes=groupesService.findAll();
    	List<Etudiant> etudiants=etudiantsService.findAll();
    	model.addAttribute("etudiants", etudiants);
    	model.addAttribute("groupes", groupes);
        return "list";
    }
    
    // Acces à l'interface du personnel
    @GetMapping("/inscription")
    public String inscription(Model model,HttpSession session) {
        if (session.getAttribute("infos")!=null){
            model.addAttribute("info",session.getAttribute("infos"));
            session.removeAttribute("infos");
        }
    	Etudiant etudiant=new Etudiant();
    	model.addAttribute("etudiant", etudiant);
        return "inscription";
    }
    
    //Les méthode de répuration post du formulaire
    
    @PostMapping("/inscription")
    public String inscriptionForm(Model model, Etudiant etudiant,@RequestParam("login") String login,@RequestParam("password") String password,HttpSession session) {
    	etudiantsService.save(etudiant);
    	model.addAttribute("etudiant", etudiant);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		Users user=new Users(login,password);
		user.setActive(true);
		user.setEtudiant(etudiant);
		user.setUsername(login);
		user.setPassword(encoder.encode(password));
		Roles role=rolesService.findOne("GESTIONNAIRE");
        Users savedUser = usersService.save(user);
        savedUser.addFirstRole(role);
        session.setAttribute("infos", "Enregistrement effectuer");
        usersService.save(savedUser);
        return "redirect:/inscription";
    }

    // Acces à l'interface d'aministration
    @GetMapping("/admin")
    public String administration(Model model) {
        return "administration/administration";
    }

    // Acces à l'interface du personnel
    @GetMapping("/etudiant")
    public String etudiant(Model model,HttpSession session) {
        if (session.getAttribute("infos")!=null){
            model.addAttribute("info",session.getAttribute("infos"));
            session.removeAttribute("infos");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user=usersRepository.findUsersByLogin(auth.getName());
        Etudiant etudiant=user.getEtudiant();
        if (etudiant==null)
            etudiant=new Etudiant();
        if(etudiant.getGroupe()!=null){
            model.addAttribute("choix",etudiant.getGroupe());
        }else{
            List<Groupe> groupes=groupesService.findActive();
            model.addAttribute("groupes", groupes);
        }
        return "etudiant";
    }

    @GetMapping("/groupes")
    public String groupes(Model model,HttpSession session) {
        if (session.getAttribute("infos")!=null){
                model.addAttribute("info",session.getAttribute("infos"));
                session.removeAttribute("infos");
        }
        Groupe groupe=new Groupe();
        model.addAttribute("groupe", groupe);
        return "groupe";
    }
    @PostMapping("/groupes")
    public String saveGroupes(Model model, Groupe groupe, HttpSession session) {
        groupe.setPoint(0);
        groupesService.save(groupe);
        session.setAttribute("infos","enregistrement du groupe effectuer");
        return "redirect:/groupes";
    }

    @PostMapping("/choix")
    public String choix(Model model,@RequestParam("choix") Integer choix, HttpSession session) {
        List<Groupe> groupes=groupesService.findActive();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user=usersRepository.findUsersByLogin(auth.getName());
        Etudiant etudiant=user.getEtudiant();
        switch (choix){
            case 1:
                etudiant.setGroupe(groupes.get(0));
                groupes.get(0).setPoint(groupes.get(0).getPoint()+1);
                groupesService.save(groupes.get(0));
                break;
            default :
                etudiant.setGroupe(groupes.get(choix-1));
                groupes.get(choix-1).setPoint(groupes.get(choix-1).getPoint()+1);
                groupesService.save(groupes.get(choix-1));
                    break;
        }
        etudiantsService.save(etudiant);
        session.setAttribute("infos","Choix Enregistrer");
        return "redirect:/etudiant";
    }

}
